package com.bjacob.myApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.trace.http.HttpTraceRepository;
import org.springframework.boot.actuate.trace.http.InMemoryHttpTraceRepository;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MyApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyApiApplication.class, args);
	}
	
	@Bean
	public HttpTraceRepository htttpTraceRepository()
	{
	  InMemoryHttpTraceRepository repo = new InMemoryHttpTraceRepository();
	  repo.setCapacity(10000);
	  return repo;
	}

}
