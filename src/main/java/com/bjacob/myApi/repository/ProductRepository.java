package com.bjacob.myApi.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.bjacob.myApi.model.Product;

public interface ProductRepository extends CrudRepository<Product, Long>{
	
	Product findById(long id);
	
	List<Product> findByName(String name);
	
	List<Product> findAll();
}
