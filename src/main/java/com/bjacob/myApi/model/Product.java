package com.bjacob.myApi.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;


@Data
@Entity
public class Product {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String name;
	
	private BigDecimal price;
	
	private String info1;
	
	private String info2;
	
	private String info3;
	
	private String info4;
	
	private String info5;
	
	

}
