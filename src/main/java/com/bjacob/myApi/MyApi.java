package com.bjacob.myApi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.trace.http.HttpTraceRepository;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bjacob.myApi.model.Product;
import com.bjacob.myApi.repository.ProductRepository;

@RestController
public class MyApi {
	
	@Autowired
	private ProductRepository repository;
	
	@Autowired
	private HttpTraceRepository httpTraceRepository;
	
	@RequestMapping("product/getAll")
	public List<Product> getAll(){
		return repository.findAll();
		
	}
	
	@RequestMapping("product/search/{name}")
	public List<Product> search(@PathVariable String name){
		return repository.findByName(name);
		
	}
	
	@RequestMapping(value="product/create",method = RequestMethod.POST)
	public Long createProduct(@RequestBody Product product) {
		
		return repository.save(product).getId();
	}
	
	@RequestMapping(value="product/createAll",method = RequestMethod.POST)
	public void createAllProduct(@RequestBody List<Product> products) {
		
		repository.saveAll(products);
	}
	
	@RequestMapping(value="trace/averageTime",method = RequestMethod.GET)
	public double getRequestMeanTime() {
		double ret = httpTraceRepository.findAll().stream().mapToLong(t->t.getTimeTaken()).average().orElse(0);
		
		return ret;
	}

}
